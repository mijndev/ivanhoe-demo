# README #

### Installation ###

It won't work without a web server due to cross origin requests, so you need a static server in order to run it. 

To run static server use the following commands:

* npm run install-global (sometimes you will need root access rights)
* cd into the project root
* static-server

To install vendor packages run bower install.
That's it.

### Who do I talk to? ###

Repo owner or admin