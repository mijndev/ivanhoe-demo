'use strict';

/**
 * Config constant
 */
app.constant('APP_MEDIAQUERY', {
    'desktopXL': 1200,
    'desktop': 992,
    'tablet': 768,
    'mobile': 480
});
app.constant('JS_REQUIRES', {
    //*** Scripts
    scripts: {
        //*** Javascript Plugins
        'd3': 'assets/bower_components/d3/d3.min.js',

        //*** jQuery Plugins
        'chartjs': 'assets/bower_components/chartjs/Chart.min.js',
        'ckeditor-plugin': 'assets/bower_components/ckeditor/ckeditor.js',
        'jquery-nestable-plugin': ['assets/bower_components/jquery-nestable/jquery.nestable.js'],
        'touchspin-plugin': ['assets/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js', 'assets/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
        'jquery-appear-plugin': ['assets/bower_components/jquery-appear/build/jquery.appear.min.js'],
        'spectrum-plugin': ['assets/bower_components/spectrum/spectrum.js', 'assets/bower_components/spectrum/spectrum.css'],

        //*** Controllers
        'transactionCtrl': 'assets/js/controllers/transactionCtrl.js',
    },
    //*** angularJS Modules
    modules: [{
        name: 'toaster',
        files: ['assets/bower_components/AngularJS-Toaster/toaster.js', 'assets/bower_components/AngularJS-Toaster/toaster.css']
    }, {
        name: 'angularBootstrapNavTree',
        files: ['assets/bower_components/angular-bootstrap-nav-tree/dist/abn_tree_directive.js', 'assets/bower_components/angular-bootstrap-nav-tree/dist/abn_tree.css']
    }, {
        name: 'ngTable',
        files: ['assets/bower_components/ng-table/dist/ng-table.min.js', 'assets/bower_components/ng-table/dist/ng-table.min.css']
    }, {
        name: 'ui.mask',
        files: ['assets/bower_components/angular-ui-utils/mask.min.js']
    }, {
        name: 'ngImgCrop',
        files: ['assets/bower_components/ngImgCrop/compile/minified/ng-img-crop.js', 'assets/bower_components/ngImgCrop/compile/minified/ng-img-crop.css']
    }, {
        name: 'angularFileUpload',
        files: ['assets/bower_components/angular-file-upload/angular-file-upload.min.js']
    }, {
        name: 'monospaced.elastic',
        files: ['assets/bower_components/angular-elastic/elastic.js']
    }, {
        name: 'ngMap',
        files: ['assets/bower_components/ngmap/build/scripts/ng-map.min.js']
    }, {
        name: 'chart.js',
        files: ['assets//bower_components/angular-chart.js/dist/angular-chart.min.js', 'assets//bower_components/angular-chart.js/dist/angular-chart.min.css']
    }, {
        name: 'flow',
        files: ['assets/bower_components/ng-flow/dist/ng-flow-standalone.min.js']
    }, {
        name: 'ckeditor',
        files: ['assets/bower_components/angular-ckeditor/angular-ckeditor.min.js']
    }, {
        name: 'mwl.calendar',
        files: ['assets/bower_components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.js', 'assets/bower_components/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css', 'assets/js/config/config-calendar.js']
    }, {
        name: 'ng-nestable',
        files: ['assets/bower_components/ng-nestable/src/angular-nestable.js']
    }, {
        name: 'ngNotify',
        files: ['assets/bower_components/ng-notify/dist/ng-notify.min.js', 'assets/bower_components/ng-notify/dist/ng-notify.min.css']
    }, {
        name: 'xeditable',
        files: ['assets/bower_components/angular-xeditable/dist/js/xeditable.min.js', 'assets/bower_components/angular-xeditable/dist/css/xeditable.css', 'assets/js/config/config-xeditable.js']
    }, {
        name: 'checklist-model',
        files: ['assets/bower_components/checklist-model/checklist-model.js']
    }, {
        name: 'ui.knob',
        files: ['assets/bower_components/ng-knob/dist/ng-knob.min.js']
    }, {
        name: 'ngAppear',
        files: ['assets/bower_components/angular-appear/build/angular-appear.min.js']
    }, {
        name: 'countTo',
        files: ['assets/bower_components/angular-count-to-0.1.1/dist/angular-filter-count-to.min.js']
    }, {
        name: 'angularSpectrumColorpicker',
        files: ['assets/bower_components/angular-spectrum-colorpicker/dist/angular-spectrum-colorpicker.min.js']
    }]
});