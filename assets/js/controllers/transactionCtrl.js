app.controller('TransactionCtrl', ['$scope', 'TransactionService', 'WebsiteService', 'AdvertisersService', 'AffiliateNetworksService',
    function($scope, TransactionService, WebsiteService, AdvertisersService, AffiliateNetworksService){

    WebsiteService.query(function(response){
       $scope.websites =  response;
    });

    AdvertisersService.query(function(response){
        $scope.advertisers = response;
    });

    AffiliateNetworksService.query(function(response){
        $scope.networks = response;
    });

    $scope.records = {};

    $scope.selected = {};

    $scope.getTransactions = function(){

        TransactionService.get({

            'program_name[]': $scope.selected.program_name || [],

            'affiliate_network_name[]': $scope.selected.affiliate_network_name || [],

            'website_ids[]': $scope.selected.website_ids || [],

            page: $scope.records.current_page || 1

        }, function(response){

            $scope.records = response;

        });
    };

    $scope.getTransactions();

    $scope.statusClass = function (id){

        var statuses = {
            1: "label-success",
            2: "label-warning",
            3: "label-error"
        };

        return statuses[id];
    }

}]);
